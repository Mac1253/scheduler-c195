package c195.database;
import c195.model.appointmentFormModel;
import c195.model.appointmentTableModel;
import c195.model.customerTableModel;
import c195.model.appointmentAlertModel;
import java.lang.reflect.InvocationTargetException;
import javafx.collections.ObservableList;
import util.DBconnection;
import java.sql.*;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import javafx.collections.FXCollections;
import c195.views.ReportsController;
/**
 *
 * @author buger
 */
public class appointmentDB {
    
    private static ObservableList<appointmentFormModel> customers = FXCollections.observableArrayList();
    private static ObservableList<appointmentTableModel> appointments = FXCollections.observableArrayList();
    
    // getCustomersForForm returns a list of customers and associated data for appointment form
    public static ObservableList<appointmentFormModel> getCustomersForForm() {
        try {            
            String query = "SELECT customerId, customerName FROM customer";
            
            Statement statement = DBconnection.getDBConnection().createStatement();
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                appointmentFormModel singleCust = new appointmentFormModel(
                    rs.getInt("customerId"), 
                    rs.getString("customerName")              
                    );
                customers.add(singleCust);
            }
        } catch (SQLException e){
            System.out.println(e);
        }
        return customers;
    }
    
    public static boolean createAppointment(int customerId, String title, String description, String location, 
            String contact, String type, String startTime, String endTime, String date) {
        try {
        String start = createTimeStamp(date, startTime, location );
        String end = createTimeStamp(date, endTime, location );
        
        String values = ""+ customerId +",'"+ 1 +"', '" + title +"', '" + description +"', '" + location + " :" + ZoneId.systemDefault() + "', '" + contact +"', '" + type + "', '" + start + "', '" + end + "', NOW(), NOW(), '" + "NULL" + "', '" + contact +"', '" + contact +"'";
        String query = "INSERT INTO appointment (customerId, userId, title, description, location, contact, type, start, end, createDate, lastUpdate, url, createdBy, lastUpdateBy) VALUES (" + values + ")" ;
        String apptQuery  = "SELECT a.* FROM appointment a LEFT JOIN customer c on a.customerId = c.customerId   WHERE a.start BETWEEN '" + start +"' AND '" + end +"' AND c.customerId= " + customerId + " OR a.contact = '" + contact + "' AND  start between '" + start + "' AND '" + end + "'";
        Statement statement = DBconnection.getDBConnection().createStatement();
        ResultSet rs = statement.executeQuery(apptQuery);
          if (rs.next()) {            
           return false;
        }
        statement.executeUpdate(query);        

        System.out.println(apptQuery);
      
        statement.close();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }  
       return true;
    }
    
    public static ObservableList<appointmentTableModel> getAppointments() {
        // getAppointments returns month view of appointments. The next 30 days out not the month of 
        try {
            LocalDate monthStart = LocalDate.now(); 
            
            LocalDate monthEnd = LocalDate.now().plusMonths(1);  

            String query = "SELECT c.customerName, a.customerId, a.start, a.end, a.location, a.contact, a.description, a.title, a.type, a.appointmentId  FROM appointment a LEFT JOIN customer c on a.customerId = c.customerId WHERE  start >= '" + monthStart + "' AND start <= '" + monthEnd + "'";
            Statement statement = DBconnection.getDBConnection().createStatement();
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {            
                appointmentTableModel atm = new appointmentTableModel(
                        rs.getInt("appointmentId"),
                        rs.getInt("customerId"),
                        rs.getString("customerName"),
                        rs.getDate("start").toString(),
                        splitTime(rs.getString("start"), rs.getString("location")),
                        splitTime(rs.getString("end"), rs.getString("location")),                        
                        rs.getString("title"),
                        rs.getString("location").split(":")[0],
                        rs.getString("description"),
                        rs.getString("type"),
                        rs.getString("contact")
                );
                appointments.add(atm);                
            }
            statement.close();
            return appointments;
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
    }
    
     public static ObservableList<appointmentTableModel> getWeekAppointments() {
        // getAppointments returns week view of appointments. The next 7 days not the week of.
        try {
            LocalDate weekStart = LocalDate.now(); 
            
            LocalDate weekEnd = LocalDate.now().plusWeeks(1);                  
            String query = "SELECT c.customerName, a.customerId, a.start, a.end, a.location, a.contact, a.description, a.title, a.type, a.appointmentId  FROM appointment a LEFT JOIN customer c on a.customerId = c.customerId WHERE  start >= '" + weekStart + "' AND start <= '" + weekEnd + "'";
            Statement statement = DBconnection.getDBConnection().createStatement();
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {                
                appointmentTableModel atm = new appointmentTableModel(
                        rs.getInt("appointmentId"),
                        rs.getInt("customerId"),
                        rs.getString("customerName"),
                        rs.getDate("start").toString(),
                        splitTime(rs.getString("start"), rs.getString("location")),
                        splitTime(rs.getString("end"), rs.getString("location")),
                        rs.getString("title"),
                        rs.getString("location").split(":")[0],
                        rs.getString("description"),
                        rs.getString("type"),
                        rs.getString("contact")
                );                
                appointments.add(atm);
            }
            statement.close();
            return appointments;
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
    }
    
    public static boolean updateAppointment(int appointmentId, int customerId, String title, String description, String location, 
            String contact, String type, String startTime, String endTime, String date) {
      try {
            String start = createTimeStamp(date, startTime, location );
            String end = createTimeStamp(date, endTime, location );
            String cQuery = "SELECT a.appointmentId FROM  appointment a LEFT JOIN customer c on a.customerId = c.customerId WHERE start between '" + start + "' AND '" + end + "' AND c.customerId = " + customerId + " OR a.contact = '" + contact + "' AND  start between '" + start + "' AND '" + end + "'";
            
            String query = "UPDATE appointment SET title = '" + title + "' , description = '" + description + "', location = '" + location + " :" + ZoneId.systemDefault() + "' , contact = '"+ contact+ "' , type = '" + type + "' , lastUpdate = NOW(), lastUpdateBy = '" + contact + "', start = '" + start + "', end = '" + end + "'  WHERE appointmentId = '" + appointmentId + "'";
            Statement statement = DBconnection.getDBConnection().createStatement();
            ResultSet rs = statement.executeQuery(cQuery);
            if (rs.next()) {
                // this means a row was found so it should return and not update the appointment.
                return false;
            }
            statement.executeUpdate(query);            
            statement.close();
            ReportsController rc = new ReportsController();
            rc.getMonthlyApptReport();
            return true;
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
    }
    
    public static boolean deleteAppointment(int appointmentId) {
        try {           
            String query = "DELETE FROM appointment WHERE appointmentId = " + appointmentId;
            Statement statement = DBconnection.getDBConnection().createStatement();
            statement.executeUpdate(query);      
            statement.close();      
        } catch (SQLException e) {
            System.out.println(e);     
            return false;
        }       
        return true;
    }
    
    
    public static appointmentAlertModel checkAppointments(int userId) {
    // return appointment if there is one within 15 minutes         
        LocalDateTime today = LocalDateTime.now();
        ZoneId zone = ZoneId.systemDefault();
        ZonedDateTime zoneTime = LocalDateTime.now().atZone(zone);
        today = zoneTime.withZoneSameLocal(ZoneId.of("UTC")).toLocalDateTime();
        LocalDateTime today15 = LocalDateTime.now().plusMinutes(15);
        appointmentAlertModel appointment =  null;
        try {
            Statement statement = DBconnection.getDBConnection().createStatement();            
            String query = "SELECT c.customerName, a.customerId, a.start, a.end, a.location, a.contact, a.description, a.title, a.type, a.appointmentId  FROM appointment a LEFT JOIN customer c on a.customerId = c.customerId WHERE  start between '" + today + "' and '" + today15 + "' and userId = " + userId;
            ResultSet rs = statement.executeQuery(query);
            while(rs.next()) {                              
                 appointment =  new appointmentAlertModel(
                            rs.getInt("customerId"), 
                            rs.getString("customerName"), 
                            rs.getString("location"));                          
            }
            statement.close();
            return appointment;
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }   
        
    }
    
    public static String splitTime(String time, String location) { 
        // location is used in adjusting times to the proper time zone        
        String adjustTime = adjustTimes(location, time); 
        String t = "";
        t = adjustTime.substring(11, 16);
        int h = Integer.parseInt(adjustTime.substring(11, 13));
        if (h >= 12) {            
            if (h > 12) {
                h = h - 12;
            }
                t =  h + ":00 PM";       
            } else {
            t =  t + " AM";
        }       
        return t;
    }
    
    public static String adjustTimes(String timeLocation, String date) {
    // Adjusts times for appointment to systems default times. Adjusts for dayligh savings as well.
        timeLocation = timeLocation.split(":")[1];
        timeLocation = timeLocation.replaceAll(" ", "");
        ZoneId z = ZoneId.systemDefault();
        ZoneId apptZone;
        apptZone = ZoneId.of(timeLocation.toString());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
        LocalDateTime apptDate = LocalDateTime.parse(date, formatter);
        ZonedDateTime adjustedTime = ZonedDateTime.of(apptDate, apptZone).withZoneSameInstant(ZoneId.of(z.toString()));
        String ldtString = adjustedTime.toLocalDateTime().format(formatter);         
        return ldtString;
        
        

    }
    
     public static String createTimeStamp(String date, String time, String location) {
        String h = time.split(":")[0];
        int hours = Integer.parseInt(h);   
        if (hours < 8) {
            hours += 12;
        }
        String newDate = String.format("%s %02d:%s", date, hours, "00");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd kk:mm");
        LocalDateTime ldt = LocalDateTime.parse(newDate, formatter);  
        Timestamp ts = Timestamp.valueOf(ldt);     
        return ts.toString();
    }

}
