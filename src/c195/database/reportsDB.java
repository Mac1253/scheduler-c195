package c195.database;
import c195.model.appointmentFormModel;
import c195.model.appointmentTableModel;
import c195.model.customerTableModel;
import c195.model.appointmentAlertModel;
import c195.model.consultantScheduleModel;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javafx.collections.ObservableList;
import util.DBconnection;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import javafx.collections.FXCollections;
/**
 *
 * @author buger
 */
public class reportsDB {
    
    private static ObservableList<consultantScheduleModel> consultants = FXCollections.observableArrayList();

  public static ObservableList<String> getConsultants() {
     try {            
            LocalDateTime today =  LocalDateTime.now();           
            String query = "SELECT distinct contact FROM appointment" ;
            
            Statement statement = DBconnection.getDBConnection().createStatement();
            ResultSet rs = statement.executeQuery(query);     
            ObservableList<String> consultants = FXCollections.observableArrayList();
            while (rs.next()) {                
                consultants.add(rs.getString("contact"));
            }          
            return consultants;
        } catch (SQLException e){
            System.out.println(e);
            return null;
        }    
    }
  
  
    public static ObservableList<consultantScheduleModel> getConsultantSchedule(String consultantName) {
        // Gets a consultants schedule.
     try {            
            LocalDateTime today =  LocalDateTime.now();           
            String query = "SELECT c.customerName, a.customerId, a.start, a.end, a.location, a.contact, a.description, a.title, a.type, a.appointmentId  FROM appointment a LEFT JOIN customer c on a.customerId = c.customerId WHERE  start > '" + today + "' AND a.contact= '" + consultantName + "'";                        
            Statement statement = DBconnection.getDBConnection().createStatement();
            ResultSet rs = statement.executeQuery(query);     
            while (rs.next()) {
                 consultantScheduleModel consultant = new consultantScheduleModel(
                        rs.getInt("appointmentId"),
                        rs.getInt("customerId"),
                        rs.getString("customerName"),
                        rs.getDate("start").toString(),
                        splitTime(rs.getString("start")),
                        splitTime(rs.getString("end")),
                        rs.getString("title"),
                        rs.getString("location").split(":")[0],
                        rs.getString("description"),
                        rs.getString("type"),
                        rs.getString("contact")
                ); 
                 consultants.add(consultant);                 
            }          
            statement.close();
            return consultants;
        } catch (SQLException e){
            System.out.println(e);
            return null;
        }    
    }
    
    public static String[] getStats() throws FileNotFoundException {
    // A report for some interesting stats. For problem I.
     String countryCount = "0";
     String appointmentCount = "0";    
     BufferedReader reader = new BufferedReader(new FileReader("logger.txt"));
     int lines = 0;
    
    try{       
        while (reader.readLine() != null) lines++;
        reader.close();
    } catch (IOException IOE) {
        System.out.println(IOE);
    }
    
         try {            
             // returns the total distinct countries in the DB
            String countryQuery = "SELECT Count(distinct country) as countryCount FROM country" ;
            // returns the total amount of appointments
            String totalApptsQuery = "SELECT Count(appointmentId) as appointmentCount FROM appointment";
            Statement statement = DBconnection.getDBConnection().createStatement();
            ResultSet rs = statement.executeQuery(countryQuery);                 
            
            if (rs.next()) {
            countryCount = rs.getString("countryCount");
            }
            
            rs = statement.executeQuery(totalApptsQuery);
            if (rs.next()) {
            appointmentCount = rs.getString("appointmentCount");
            }
            
            statement.close();
        } catch (SQLException e){
            System.out.println(e);
            return null;
        }    
    String[] stats = {countryCount, appointmentCount, String.valueOf(lines)};
    return  stats;
    }
    
     public static int[] getAppointmentReport() {
     
        try {            
            LocalDateTime today =  LocalDateTime.now(); 
            // 08/27/2020 -  EVALUATOR COMMENTS: ATTEMPT 2: The application includes functionality to generate reports. The "number of appointment types by month" report displayed all zeroes when there were four different appointments.
            // The above comment was made by an Evalutor. The application is supposed to do the next 30 days from the day of use to get the appointments
            // For attempt 3 ill make it so all appointments are always counted. It was previously only the next 30 days 
            String query = "SELECT type FROM appointment";
            Statement statement = DBconnection.getDBConnection().createStatement();
            ResultSet rs = statement.executeQuery(query);
            // New Prospect, Evaluation, Legal, and Other map respectively to the apptTypes array
            int[] apptTypes = {0,0,0,0};
            
            while (rs.next()) {
                switch(rs.getString("type")) {
                    case "New Prospect":                        
                        apptTypes[0] = apptTypes[0] + 1;
                        break;
                    case "Evaluation":           
                        apptTypes[1] = apptTypes[1] + 1;
                        break;
                    case "Legal":
                        apptTypes[2] = apptTypes[2] + 1;
                        break;
                    case "Other":                        
                        apptTypes[3] = apptTypes[3] + 1;    
                        break;
                }                        
            }          
            return apptTypes;
        } catch (SQLException e){
            System.out.println(e);
            return null;
        }           
    }
     
     public static String splitTime(String time) {       
        String t = "";
        t = time.substring(11, 16);
        int h = Integer.parseInt(time.substring(11, 13));
        if (h >= 12) {        
        if (h > 12) {h = h - 12;}
        t =  h + ":00 PM";       
        } else {
        t =  t + " AM";
        }
        
       
        return t;
    }
}
