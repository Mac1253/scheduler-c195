package c195.database;
import c195.model.customerTableModel;
import javafx.collections.ObservableList;
import util.DBconnection;
import java.sql.*;
import javafx.collections.FXCollections;

/**
 *
 * @author buger
 */
public class customerDB {
    private static ObservableList<customerTableModel> customers = FXCollections.observableArrayList();

    public static ObservableList<customerTableModel> getCustomers() {
        try {
            String query = "SELECT c.customerId, c.customerName, a.address, a.address2, a.postalCode, a.phone, city.city, country.country\n" +
            "FROM customer c\n" +
            "LEFT JOIN address a on c.addressId = a.addressId\n" +
            "LEFT JOIN city on a.cityId= city.cityId\n" +
            "LEFT JOIN country on city.countryId = country.countryId";
            
            Statement statement = DBconnection.getDBConnection().createStatement();
            ResultSet rs = statement.executeQuery(query);
            while(rs.next()) {
                customerTableModel singleCust = new customerTableModel(
                    rs.getInt("customerId"), 
                    rs.getString("customerName"), 
                    rs.getString("address"),
                    rs.getString("address2"),
                    rs.getString("city"),
                    rs.getString("phone"),
                    rs.getString("country"),
                    rs.getString("postalCode")
                    );
                customers.add(singleCust);
            }
            statement.close();
            return customers;
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
    }
    
    
    public static void createCustomer(String name, String address, String secondAddress, String city, 
            String zipCode, String country, String phone) throws SQLException {        
        try {
        String countryQuery = "INSERT INTO country SET  lastUpdateBy=NOW(), createdBy=NOW(),createDate=NOW(), country='" + country + "'";
        PreparedStatement statement = DBconnection.getDBConnection().prepareStatement(countryQuery, Statement.RETURN_GENERATED_KEYS);
//        Statement statement = DBconnection.getDBConnection().createStatement();
        statement.executeUpdate();
        ResultSet rs = statement.getGeneratedKeys();
        int countryId =  0;
        if (rs.next()) {            
            countryId = rs.getInt(1);
        }
        String cityQuery = "";
        if (countryId > 0) {
            cityQuery = "INSERT INTO city SET lastUpdateBy=NOW(), createdBy=NOW(),createDate=NOW(), city='" + city +"'" + ", countryId=" + countryId;
        }
        statement.executeUpdate(cityQuery, Statement.RETURN_GENERATED_KEYS);
        rs = statement.getGeneratedKeys();
        int cityId =  0;
        if(rs.next()) {            
            cityId = rs.getInt(1);
        }
        
        String addressQuery = "Insert INTO address SET lastUpdateBy=NOW(), createdBy=NOW(),createDate=NOW(), address='" + address +  "', address2='" + secondAddress +  "', phone='" + phone +"', postalCode='" + zipCode + "', cityId='" + cityId + "'";
        statement.executeUpdate(addressQuery, Statement.RETURN_GENERATED_KEYS);
        rs = statement.getGeneratedKeys();
        int addressId =  0;
        if(rs.next()) {            
            addressId = rs.getInt(1);
        }
    
        String customerQuery = "INSERT INTO customer SET lastUpdateBy=NOW(), createdBy=NOW(),createDate=NOW(), customerName='" + name +"', addressId='" + addressId +"', active='" + 1 +"'";
        statement.executeUpdate(customerQuery);
        
        } 
        catch (SQLException e) {
        System.out.println(e);
        }                
    }
    
    public static boolean updateCustomer(String name, String address, String secondAddress, String city, 
            String zipCode, String country, String phone, int custId) {        
     try {
            Statement statement = DBconnection.getDBConnection().createStatement();
            String idQuery = "SELECT c.customerId, a.addressId, city.cityId, country.countryId\n" +
            "FROM customer c \n" +
            "LEFT JOIN address a on c.addressId = a.addressId \n" +
            "LEFT JOIN city on a.cityId= city.cityId \n" +
            "LEFT JOIN country on city.countryId = country.countryId\n" +
            "where c.customerId = " + custId;
            ResultSet rs = statement.executeQuery(idQuery);
            rs.next();
            int aId = rs.getInt("addressId");
            int cId = rs.getInt("cityId");
            int cyId = rs.getInt("countryId");
            String custQuery = "UPDATE customer SET customerName = '" + name + "', lastUpdate=NOW() WHERE customerId =" + custId;
            String addressQuery = "UPDATE address SET address = '"+ address +"', address2 = '"+ secondAddress +"', postalCode='"+ zipCode +"', phone='"+ phone +"',lastUpdate=NOW()"
                    + " WHERE addressId=" + aId;
            String cityQuery = "UPDATE city SET city = '" + city + "', lastUpdate=NOW() WHERE cityId=" + cId;
            String countryQuery = "UPDATE country SET country= '" + country+ "', lastUpdate=NOW() WHERE countryId=" + cyId;
            
            statement.executeUpdate(custQuery);
            statement.executeUpdate(addressQuery);
            statement.executeUpdate(cityQuery);
            statement.executeUpdate(countryQuery);       
         } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
            return true;
    }
    
     public static boolean deleteCustomer(int custId) {
    
     try {
  
            String query = "DELETE FROM customer WHERE customer.customerId = " + custId;
            
            Statement statement = DBconnection.getDBConnection().createStatement();
            statement.executeUpdate(query);
           
         } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
            return true;
     }
    
}
