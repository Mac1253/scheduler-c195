/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195.views;

import c195.database.appointmentDB;
import c195.model.appointmentFormModel;
import c195.model.appointmentTableModel;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.time.*;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author buger
 */
public class CalendarController implements Initializable {
    
    @FXML
    private AnchorPane appointmentFormPane;
    @FXML
    private ComboBox<appointmentFormModel> selectCustomerCB;
    @FXML
    private ComboBox contactCB;
    @FXML
    private ComboBox startTime;
    @FXML
    private ComboBox endTime;
    @FXML
    private DatePicker apptDate;
    @FXML
    private TextField title;
    @FXML
    private TextField description;
    @FXML
    private TextField location;
    @FXML
    private ComboBox type;
    @FXML
    private TableView<appointmentTableModel> appointmentTable;
    @FXML
    private TableColumn apptCustIdCol;
    @FXML
    private TableColumn apptNameCol;
    @FXML
    private TableColumn apptDateCol;
    @FXML
    private TableColumn apptStartCol;
    @FXML
    private TableColumn apptEndCol;
    @FXML
    private Label appointmentErrorLabel;
    
    @FXML
    private Button editApptButton;
    @FXML
    private Button deleteApptButton;
    @FXML
    private Button newApptButton;
    @FXML
    private Button saveEditApptButton;
    @FXML
    private Hyperlink switchViewLink;
           
    
    appointmentTableModel selectedAppointment;

    
    private ObservableList<appointmentFormModel> customersApptForm = FXCollections.observableArrayList();
    private final ObservableList<String> contacts = FXCollections.observableArrayList("Johnson", "Jane", "Bill", "Thomas", "Carol");
    private final ObservableList<String> types = FXCollections.observableArrayList("New Prospect", "Evaluation", "Legal", "Other");
    private final ObservableList<String> hours = FXCollections.observableArrayList("8:00 AM","9:00 AM","10:00 AM","11:00 AM","12:00 PM","1:00 PM","2:00 PM","3:00 PM","4:00 PM");
    // tableView: true = monthly, false = weekly
    private boolean tableView = true;
    
    public void swtichView() {
        tableView = !tableView;
        switchViewLink.setText("Swtich to monthly view");
        if (tableView) { 
            refreshTable();
        } else {
            refreshTable();
        }
    }
    
    public void onNewAppointment() {
        appointmentFormPane.disableProperty().set(false);
        // lambdas function. Disables previous dates before today. Faster than checking for input validation later.
         apptDate.setDayCellFactory(picker -> new DateCell() {
        public void updateItem(LocalDate date, boolean empty) {
            super.updateItem(date, empty);
            LocalDate today = LocalDate.now();

            setDisable(empty || date.compareTo(today) < 0 );
        }
    });
    }
    
    public void getCustomers() {
        customersApptForm = appointmentDB.getCustomersForForm();
     
        selectCustomerCB.setItems(customersApptForm);
        selectCustomerCB.setConverter(new StringConverter<appointmentFormModel>(){
            @Override
            public String toString(appointmentFormModel am) {
                return am.getCustName() + "- ID: " + am.getCustId();
            }
            @Override
            public appointmentFormModel fromString(String string) {
                return null;
            }
        });        
        
    }
    
    @FXML
    public void onCreateAppointment() throws SQLException  {
       
//        appointmentDB.createAppointment(selectCustomerCB.getValue().getCustId(), title.getText(), description.getText(), location.getText(), contactCB.getValue().toString(), type.getValue().toString(), startTime.getValue().toString(), endTime.getValue().toString());
        LocalDate localDate = apptDate.getValue();
        
        if (inputValidation()) {
              boolean rs = appointmentDB.createAppointment(selectCustomerCB.getValue().getCustId(), title.getText(), description.getText(), location.getText(), contactCB.getValue().toString(), 
                type.getValue().toString(), startTime.getValue().toString(), endTime.getValue().toString(), localDate.toString());
                if (rs) {
                    refreshTable();
                    appointmentFormPane.disableProperty().set(true);
                    newApptButton.visibleProperty().set(true);
                    newApptButton.disableProperty().set(false);
                    saveEditApptButton.visibleProperty().set(false);        
                    saveEditApptButton.disableProperty().set(true);        
                    appointmentFormPane.disableProperty().set(true);
                    editApptButton.disableProperty().set(true);
                    deleteApptButton.disableProperty().set(true);
                    clearForm();
        } else {
           appointmentErrorLabel.setText("Appointment overlap!");            
        }     
        } 
  
    }   
    
    public boolean inputValidation () {
        // check all inputs for valies, give error code if empty or null.   
        int sti = startTime.getSelectionModel().getSelectedIndex();
        int eti = endTime.getSelectionModel().getSelectedIndex();
      
        if(title.getText().equals("") || title.getText() == null ) {
            appointmentErrorLabel.setText("Please enter a valid title");
            return false;
        } else
        
        if(description.getText().equals("") || description.getText().equals(" ") || description.getText() == null ) {
            appointmentErrorLabel.setText("Please enter a valid description");
            return false;
        } else 
        
        // cust second address is optional
        
        if(location.getText().equals("") || location.getText().equals(" ")  || location.getText() == null ) {
            appointmentErrorLabel.setText("Please enter a valid location");            
            return false;
        } else
        
        if(type.getSelectionModel().isEmpty() || type == null ) {
            appointmentErrorLabel.setText("Please select an appointment type");
            return false;
        } else
        
        if(apptDate.getValue() == null) {
            appointmentErrorLabel.setText("Please select a valid date");
            return false;
         }  else
            
        if(contactCB.getSelectionModel().isEmpty() || contactCB == null ) {
            appointmentErrorLabel.setText("Please select a contact/consultant");
            return false;
         } else 
            
        if (startTime.getSelectionModel().isEmpty() || startTime == null  ) {          
            appointmentErrorLabel.setText("Please select a valid start time");
            return false;
        } else 
            
        if (endTime.getSelectionModel().isEmpty() || endTime == null  ) {        
            appointmentErrorLabel.setText("Please select a valid end time");
            return false;
        } else
        if (sti >= eti) {
            appointmentErrorLabel.setText("Start time must be before the end time"); 
            return false;
            } else 
        if (location.getText().contains(":")) {
            location.setText(location.getText().replace(":", "--"));
        }
        
            
            appointmentErrorLabel.setText("");
            return true;
//        
////    lambdas func  
////    textField.textProperty().addListener((observable, oldValue, newValue) ->
////    System.out.println("Input Validation"));
    }
    
    
    public void clearForm() {        
        apptDate.setValue(null);
        title.clear();
        description.clear();
        location.clear();
        selectCustomerCB.setValue(null);
        selectCustomerCB.setPromptText("Select Customer");
        contactCB.setValue(null);
        type.setValue(null);
        startTime.setValue(null);
        endTime.setValue(null);
    }
    
    
    public void refreshTable() {
        appointmentTable.getItems().clear();
        selectedAppointment =  new appointmentTableModel();
        fillTable();
    };
    
    public void fillTable() {
        apptCustIdCol.setCellValueFactory(new PropertyValueFactory<>("custId"));
        apptNameCol.setCellValueFactory(new PropertyValueFactory<>("custName"));
        apptDateCol.setCellValueFactory(new PropertyValueFactory<>("custDate"));
        apptStartCol.setCellValueFactory(new PropertyValueFactory<>("startTime"));
        apptEndCol.setCellValueFactory(new PropertyValueFactory<>("endTime"));
        if (tableView) {
            // Monthly
            appointmentTable.setItems(appointmentDB.getAppointments());        
        } else {
            // Weekly
            appointmentTable.setItems(appointmentDB.getWeekAppointments());        
        }
        
    }
    
      @FXML()
    public void onAppointmentSelect() {        
        if(appointmentTable.getSelectionModel().getSelectedItem() != null) {
            selectedAppointment = appointmentTable.getSelectionModel().getSelectedItem();
            editApptButton.disableProperty().set(false);
            deleteApptButton.disableProperty().set(false);           
            // clear model selection            
        } else {
            appointmentTable.getSelectionModel().clearSelection();
            editApptButton.disableProperty().set(true);
            deleteApptButton.disableProperty().set(true);
            selectedAppointment =  new appointmentTableModel();
        }
    }
    
      @FXML
    public void onEditAppointment() {     
        newApptButton.visibleProperty().set(false);
        newApptButton.disableProperty().set(true);
        saveEditApptButton.visibleProperty().set(true);        
        saveEditApptButton.disableProperty().set(false);        
        appointmentFormPane.disableProperty().set(false);
        selectCustomerCB.disableProperty().set(true);
        selectCustomerCB.setPromptText(selectedAppointment.getCustName() + "- ID: " + selectedAppointment.getCustId());        
        LocalDate date = LocalDate.parse(selectedAppointment.getCustDate());       
        apptDate.setValue(date);
        // Lambdas used again
        apptDate.setDayCellFactory(picker -> new DateCell() {
        public void updateItem(LocalDate date, boolean empty) {
            super.updateItem(date, empty);
            LocalDate today = LocalDate.now();

            setDisable(empty || date.compareTo(today) < 0 );
        }
    });
        startTime.setValue(selectedAppointment.getStartTime());
        endTime.setValue(selectedAppointment.getEndTime());
        location.setText(selectedAppointment.getLocation());
        title.setText(selectedAppointment.getTitle());
        description.setText(selectedAppointment.getDescription());
        type.setValue(selectedAppointment.getType());
        contactCB.setValue(selectedAppointment.getContact());
    }
    
    
    @FXML
    public void onUpdateAppointment() {
        if (inputValidation()) {
        LocalDate localDate = apptDate.getValue();  
        if (appointmentDB.updateAppointment(selectedAppointment.getAppointmentId(), selectedAppointment.getCustId(), title.getText(), description.getText(), location.getText(), contactCB.getValue().toString(), 
                type.getValue().toString(), startTime.getValue().toString(), endTime.getValue().toString(), localDate.toString())) {
        } else {
            appointmentErrorLabel.setText("Appointment overlap!");
            return;
        }      
        
        newApptButton.visibleProperty().set(true);
        newApptButton.disableProperty().set(false);
        saveEditApptButton.visibleProperty().set(false);        
        saveEditApptButton.disableProperty().set(true);        
        appointmentFormPane.disableProperty().set(true);
        editApptButton.disableProperty().set(true);
        deleteApptButton.disableProperty().set(true);
        selectCustomerCB.disableProperty().set(false);
        selectCustomerCB.setPromptText("Select a customer");        
        clearForm();
        refreshTable();
        }   
    }
    
    @FXML
    public void onDeleteAppointment() {        
            boolean rs = appointmentDB.deleteAppointment(selectedAppointment.getAppointmentId());    
            if (rs) {
                appointmentTable.getSelectionModel().clearSelection();
                selectedAppointment =  new appointmentTableModel();
                refreshTable();
                appointmentFormPane.disableProperty().setValue(true);
                editApptButton.disableProperty().set(true);
                deleteApptButton.disableProperty().set(true);
            }
        
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {        
        startTime.setItems(hours);
        endTime.setItems(hours);
        contactCB.setItems(contacts);
        type.setItems(types);
        getCustomers();
        fillTable();
    }    
    
}
