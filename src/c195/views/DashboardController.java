/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195.views;

import c195.database.appointmentDB;
import c195.model.appointmentAlertModel;
import c195.views.ReportsController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.Parent;
import java.io.IOException;
import static java.lang.System.out;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Tab;



/**
 * FXML Controller class
 *
 * @author Michael
 */


public class DashboardController implements Initializable {

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {          
        appointmentAlertModel appointment = appointmentDB.checkAppointments(1);
        Alert alert = new Alert(Alert.AlertType.WARNING);
        if (appointment != null) {
        alert.setTitle("15 minute Appointment Alert!");     
        alert.setContentText("You have an appointment with " + appointment.getCustName() + " within 15 minutes at: " + appointment.getLocation().split(":")[0]);
        alert.showAndWait();
        }
    }
    
}
