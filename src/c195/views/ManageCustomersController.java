/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195.views;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import java.sql.Statement;
import util.DBconnection;
import c195.database.customerDB;
import c195.model.customerTableModel;
import java.io.IOException;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Michael
 */
public class ManageCustomersController implements Initializable {

       @FXML
       private TextField custName;
       @FXML
       private TextField custAddress;
       @FXML
       private TextField custSecondAddress;
       @FXML
       private TextField custCountry;
       @FXML
       private TextField custCity;
       @FXML
       private TextField custZipCode;
       @FXML
       private TextField custPhoneNumber;
       @FXML
       private AnchorPane customerFormPane;
       @FXML
       private Label customerErrorLabel;
       @FXML
       private TableView<customerTableModel> customerTable;
       @FXML
       private TableColumn custIdCol;
       @FXML
       private TableColumn custNameCol;
       @FXML
       private TableColumn custAddressCol;
       @FXML
       private TableColumn custCityCol;
       @FXML
       private TableColumn custPhoneCol;
       @FXML
       private TableColumn custCountryCol;
       @FXML
       private Button newCustButton;
       @FXML
       private Button createCustButton;
       @FXML
       private Button deleteCustButton;
       @FXML
       private Button editCustButton;
       @FXML
       private Button updateCustButton;
       
       customerTableModel selectedCust;
    
    /**
     * Initializes the controller class.
     */
    @FXML
    public void onNewCustomer() {
        customerFormPane.disableProperty().setValue(false);
        newCustButton.disableProperty().set(true);
    }
    
    @FXML
    public void onCreateCustomer() throws SQLException {
        
        if (inputValidation()) {
            customerFormPane.disableProperty().setValue(true);
            newCustButton.disableProperty().set(false); 
            customerDB.createCustomer(custName.getText(), custAddress.getText(), 
                custSecondAddress.getText(),
                custCity.getText(), custZipCode.getText(), custCountry.getText(), 
                custPhoneNumber.getText());
      clearFields();
      refreshTable();
      customerErrorLabel.setText("");
        } else {
        return;
        }
        
        
    }
    @FXML()
    public void onCustomerSelect() {
        
          if(customerTable.getSelectionModel().getSelectedItem() != null) {
            selectedCust = customerTable.getSelectionModel().getSelectedItem();
            editCustButton.disableProperty().set(false);
            deleteCustButton.disableProperty().set(false);
            // clear model selection            
        } else {
            customerTable.getSelectionModel().clearSelection();
             editCustButton.disableProperty().set(true);
            deleteCustButton.disableProperty().set(true);
            selectedCust =  new customerTableModel();        
        }
    }
    
    public void refreshTable() {
        customerTable.getItems().clear();
        selectedCust =  new customerTableModel();
        fillTable();
    }
    
    public boolean inputValidation() {
        // check all inputs for valies, give error code if empty or null.

        if(custName.getText().equals("") || custName.getText() == null ) {
            customerErrorLabel.setText("Please enter a valid customer name");
            return false;
        } else
        
        if(custAddress.getText().equals("") || custAddress.getText().equals(" ") || custAddress.getText() == null ) {
            customerErrorLabel.setText("Please enter a valid customer address");
            return false;
        } else 
        
        // cust second address is optional
        
        if(custCountry.getText().equals("") || custCountry.getText().equals(" ")  || custCountry.getText() == null ) {
            customerErrorLabel.setText("Please enter a valid country name");            
            return false;
        } else
        
        if(custCity.getText().equals("") ||custCity.getText().equals(" ") || custCity.getText() == null ) {
            customerErrorLabel.setText("Please enter a valid city");
            return false;
        } else
        
        if(custPhoneNumber.getText().equals("") || custPhoneNumber.getText().equals(" ") || custPhoneNumber.getText() == null  || custPhoneNumber.getText().length() > 12 || custPhoneNumber.getText().length() < 7) {
            customerErrorLabel.setText("Please enter a valid phone number\n Numbers and dashes only");
            return false;
         }  else
            
        if(custZipCode.getText().equals("") || custZipCode.getText().equals(" ") || custZipCode.getText() == null || !(custZipCode.getText().length() == 5) ) {
            customerErrorLabel.setText("Please enter a valid zip code");
            return false;
         }
            
            return true;
        
    }
    
    
    
    @FXML
    public void onDeleteCustomer() {         
        int custId = selectedCust.getCustId();
       boolean confirmDel = customerDB.deleteCustomer(custId);
       if (confirmDel) {
       customerTable.getSelectionModel().clearSelection();
        selectedCust =  new customerTableModel();
       refreshTable();
         customerFormPane.disableProperty().setValue(true);
        editCustButton.disableProperty().set(true);
        deleteCustButton.disableProperty().set(true);
       }
       
    }
    
    @FXML
    public void onEditCustomer() {        
        custName.setText(selectedCust.getCustName());
        custAddress.setText(selectedCust.getCustAddress());
        custSecondAddress.setText(selectedCust.getCustAddress2());
        custCity.setText(selectedCust.getCustCity());
        custCountry.setText(selectedCust.getCustCountry());
        custPhoneNumber.setText(selectedCust.getCustPhone());
        custZipCode.setText(selectedCust.getCustZip());
        customerFormPane.disableProperty().setValue(false);
        createCustButton.visibleProperty().set(false);
        createCustButton.disableProperty().set(true);
        updateCustButton.visibleProperty().set(true);
        updateCustButton.disableProperty().set(false);
        custName.requestFocus();
    }
    
    @FXML
    public void onUpdateCustomer() {
        int custId = selectedCust.getCustId();
        customerErrorLabel.setText("");
        if (inputValidation()) {
            boolean confirmUpd = customerDB.updateCustomer(custName.getText(), custAddress.getText(), 
                custSecondAddress.getText(),
                custCity.getText(), custZipCode.getText(), custCountry.getText(), 
                custPhoneNumber.getText(), custId);
        if (confirmUpd) {
        customerTable.getSelectionModel().clearSelection();
        selectedCust =  new customerTableModel();
        refreshTable();
        clearFields();
        customerFormPane.disableProperty().setValue(true);
        editCustButton.disableProperty().set(true);
        deleteCustButton.disableProperty().set(true);
        } else {
            return;
        }        
       }
    }
    
    public void fillTable() {
        custIdCol.setCellValueFactory(new PropertyValueFactory<>("custId"));
        custNameCol.setCellValueFactory(new PropertyValueFactory<>("custName"));
        custAddressCol.setCellValueFactory(new PropertyValueFactory<>("custAddress"));
        custCityCol.setCellValueFactory(new PropertyValueFactory<>("custCity"));
        custPhoneCol.setCellValueFactory(new PropertyValueFactory<>("custPhone"));
        custCountryCol.setCellValueFactory(new PropertyValueFactory<>("custCountry"));
        customerTable.setItems(customerDB.getCustomers());
    }
    
    public void clearFields() {
       custName.clear();
       custAddress.clear();
       custSecondAddress.clear();
       custCountry.clear();
       custCity.clear();
       custZipCode.clear();
       custPhoneNumber.clear();
    }
        
       
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // Lambdas function used. Can do input validation for numbers immediately. Faster than the other methods. 
        custZipCode.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d+") || newValue.length() > 5) {
                customerErrorLabel.setText("Please enter a valid zip code");
            } else {
                customerErrorLabel.setText("");
            }
        });
        custPhoneNumber.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("-\\d+") || newValue.length() > 12) {
                customerErrorLabel.setText("Please enter a valid phone number");
            } else {
                customerErrorLabel.setText("");
            }
        });
        
        fillTable();
    }    
    
}
