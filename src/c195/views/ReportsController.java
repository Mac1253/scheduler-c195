/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195.views;

import c195.database.reportsDB;
import c195.model.consultantScheduleModel;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author buger
 */

public class ReportsController implements Initializable {
    @FXML
    private Label npCount;
    @FXML
    private Label eCount;
    @FXML
    private Label lCount;
    @FXML
    private Label oCount;
    @FXML
    private ListView consultantList;
    
    @FXML
    private Label loginLabel;
    @FXML
    private Label countryCountLabel;
    @FXML
    private Label apptCountLabel;
    
    @FXML
    private TableColumn conCustIdCol;
    @FXML
    private TableColumn conTitleCol;
    @FXML
    private TableColumn conDescriptionCol;
    @FXML
    private TableColumn conTypeCol;
    @FXML
    private TableColumn conLocationCol;
    @FXML
    private TableColumn conNameCol;
    @FXML
    private TableColumn conDateCol;
    @FXML
    private TableColumn conStartCol;
    @FXML
    private TableColumn conEndCol;
    
    @FXML
    private TableView scheduleTable;
    @FXML
    private Label consultantNameTableLabel;
    
    private String selectedConsultant;
    private final ObservableList<String> contacts = FXCollections.observableArrayList("Johnson", "Jane", "Bill", "Thomas", "Carol");
    
    @FXML
    public void onConsultantSelect() {    
        if (scheduleTable != null) {
            scheduleTable.getItems().clear();
        }        
        fillTable(consultantList.getSelectionModel().getSelectedItem().toString());
    }
    
      public void fillTable(String consultantName) {
        conCustIdCol.setCellValueFactory(new PropertyValueFactory<>("custId"));
        conTitleCol.setCellValueFactory(new PropertyValueFactory<>("title"));
        conDescriptionCol.setCellValueFactory(new PropertyValueFactory<>("description"));
        conLocationCol.setCellValueFactory(new PropertyValueFactory<>("location"));
        conTypeCol.setCellValueFactory(new PropertyValueFactory<>("type"));
        conNameCol.setCellValueFactory(new PropertyValueFactory<>("custName"));
        conDateCol.setCellValueFactory(new PropertyValueFactory<>("custDate"));
        conStartCol.setCellValueFactory(new PropertyValueFactory<>("startTime"));
        conEndCol.setCellValueFactory(new PropertyValueFactory<>("endTime"));
        consultantNameTableLabel.setText(consultantName);        
        scheduleTable.setItems(reportsDB.getConsultantSchedule(consultantName));        
        
    }
      
      public void getMonthlyApptReport () {
        int[] apptTypesCount = reportsDB.getAppointmentReport();
            consultantList.setItems(contacts);           
           npCount.setText(String.valueOf(apptTypesCount[0]));          
           eCount.setText(String.valueOf(apptTypesCount[1]));
           lCount.setText(String.valueOf(apptTypesCount[2]));
           oCount.setText(String.valueOf(apptTypesCount[3]));
      
      }
      public void getStats() {
      try {          
            String[] stats = reportsDB.getStats();    
            
            countryCountLabel.setText(stats[0]);
            apptCountLabel.setText(stats[1]);
            loginLabel.setText(stats[2]);
           } catch  (FileNotFoundException fnfe) {
               System.out.println(fnfe.toString());
           }
      }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // run reports first
        getMonthlyApptReport();
        getStats();
        
      // constantly updates reports
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {getMonthlyApptReport();getStats();});                
            }
        }, 1000,1000);

              
    }    
    
}
