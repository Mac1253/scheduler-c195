/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195;

import java.net.URL;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.Parent;
import java.io.IOException;
import c195.database.loginDB;
import c195.views.DashboardController;
import javafx.scene.Node;
import javafx.scene.control.PasswordField;
import util.Logger;
import java.awt.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;

/**
 *
 * @author Michael
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private TextField userName;
//    @FXML
//    private TextField userPassword;
    @FXML
    private PasswordField userPassword;
    @FXML
    private Button loginButton;
    @FXML
    private Label errorLabelUserName;
    @FXML
    private Label errorLabelPassword;
    
    @FXML
    private void onLogin (ActionEvent event) {
        try {
            this.loginValdition(event);
        } catch (IOException e) {
            System.out.print(e);
        }
         
        
        
    }
    
    
    public void loginValdition (ActionEvent event) throws IOException {        
        // Check login form for input validation
        String userNameLC = this.userName.getText().toLowerCase();
        String passwordLC = this.userPassword.getText().toLowerCase();
        String loginRes = loginDB.loginDBConn(userNameLC, passwordLC);
        resetLabels();
        if (loginRes == "Success") {
            // Succesful login. Load dashboard fxml.
            Logger.logSignIn(1, this.userName.getText());
            Parent parent = FXMLLoader.load(getClass().getResource("views/dashboard.fxml"));            
            Scene scene = new Scene(parent);
            Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();            
        } else if (loginRes == "Incorrect password") {
            this.errorLabelPassword.visibleProperty().set(true);
        } else if (loginRes == "Incorrect user name") {
            this.errorLabelUserName.visibleProperty().set(true);
        }
    }
    
    public void resetLabels() {
        // Reset error labels
        this.errorLabelPassword.visibleProperty().set(false);
        this.errorLabelUserName.visibleProperty().set(false);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Locale locale = Locale.getDefault();
        rb = ResourceBundle.getBundle("c195/language/login", locale);
        this.userName.setPromptText(rb.getString("userNamePromptText"));
        this.userPassword.setPromptText(rb.getString("userPasswordPromptText"));
        this.loginButton.setText(rb.getString("loginButton"));
        this.errorLabelPassword.setText(rb.getString("userPasswordError"));
        this.errorLabelUserName.setText(rb.getString("userNameError"));

        
    }    
    
}
