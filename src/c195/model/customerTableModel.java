package c195.model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author buger
 */
public final class customerTableModel {
    private final SimpleIntegerProperty custId = new SimpleIntegerProperty();
    private final SimpleStringProperty custName = new SimpleStringProperty();
    private final SimpleStringProperty custAddress = new SimpleStringProperty();
    private final SimpleStringProperty custAddress2 = new SimpleStringProperty();
    private final SimpleStringProperty custCity = new SimpleStringProperty();
    private final SimpleStringProperty custCountry = new SimpleStringProperty();
    private final SimpleStringProperty custPhone = new SimpleStringProperty();
    private final SimpleStringProperty custZip = new SimpleStringProperty();
    
        public customerTableModel() {}

        public customerTableModel(int id, String name, String address, String address2, String city, String phone, String country, String zip) {
        setCustId(id);
        setCustName(name);
        setCustAddress(address);
        setCustAddress2(address2);
        setCustCity(city);
        setCustPhone(phone);
        setCustCountry(country);
        setCustZip(zip);
    }      
        
    public void setCustAddress2(String address2)     {
        this.custAddress2.set(address2);
    }
    
    public void setCustZip(String zip)     {
        this.custZip.set(zip);
    }
    
    public void setCustId(int custId) {
        this.custId.set(custId);
    }
    
    public void setCustName(String custName) {
        this.custName.set(custName);
    }
    
    public void setCustAddress(String custAddress) {
        this.custAddress.set(custAddress);
    }
    
    public void setCustCity(String custCity) {
        this.custCity.set(custCity);
    }
    
    public void setCustPhone(String custPhone) {
        this.custPhone.set(custPhone);
    }
    
    public void setCustCountry(String custCountry) {
        this.custCountry.set(custCountry);
    }
    
    public String getCustZip() {
        return custZip.get();
    }
    
    public String getCustAddress2() {
        return custAddress2.get();
    }
        
    public int getCustId() {
        return custId.get();
    }
    
    public String getCustName() {
        return custName.get();
    }
    
    public String getCustAddress() {
        return custAddress.get();
    }
    
    public String getCustCity() {
        return custCity.get();
    }
    
    public String getCustPhone() {
        return custPhone.get();
    }
    
    public String getCustCountry() {
        return custCountry.get();
    }
    
    
    
}
