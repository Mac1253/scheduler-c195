package c195.model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author buger
 */
public final class appointmentFormModel {
    private final SimpleIntegerProperty custId = new SimpleIntegerProperty();
    private final SimpleStringProperty custName = new SimpleStringProperty();
    
     public appointmentFormModel() {}

    public appointmentFormModel(int id, String name) {
        setCustId(id);
        setCustName(name);
      
    }
        
    public void setCustId(int custId) {
        this.custId.set(custId);
    }
    
    public void setCustName(String custName) {
        this.custName.set(custName);
    }
    public int getCustId() {
        return custId.get();
    }
    
    public String getCustName() {
        return custName.get();
    }
}
