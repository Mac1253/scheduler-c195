package c195.model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author buger
 */
public final class appointmentAlertModel {
    private final SimpleIntegerProperty custId = new SimpleIntegerProperty();
    private final SimpleStringProperty custName = new SimpleStringProperty();
    private final SimpleStringProperty apptLocation = new SimpleStringProperty();
    
     public appointmentAlertModel() {}

    public appointmentAlertModel(int id, String name, String location) {
        setCustId(id);
        setCustName(name);
        setLocation(location);
    }
        
    public void setCustId(int custId) {
        this.custId.set(custId);
    }
    
    public void setCustName(String custName) {
        this.custName.set(custName);
    }
    
    public void setLocation(String location){
        this.apptLocation.set(location);
    }
    
    public int getCustId() {
        return custId.get();
    }
    
    public String getCustName() {
        return custName.get();
    }
    
    public String getLocation() {
        return apptLocation.get();
    }
}
