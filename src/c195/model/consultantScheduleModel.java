package c195.model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author buger
 */
public final class consultantScheduleModel {
    private final SimpleIntegerProperty appointmentId = new SimpleIntegerProperty();
    private final SimpleIntegerProperty custId = new SimpleIntegerProperty();
    private final SimpleStringProperty custName = new SimpleStringProperty();
    private final SimpleStringProperty custDate = new SimpleStringProperty();
    private final SimpleStringProperty startTime = new SimpleStringProperty();
    private final SimpleStringProperty endTime = new SimpleStringProperty();
    private final SimpleStringProperty title = new SimpleStringProperty();
    private final SimpleStringProperty location = new SimpleStringProperty();
    private final SimpleStringProperty description = new SimpleStringProperty();
    private final SimpleStringProperty type = new SimpleStringProperty();
    private final SimpleStringProperty contact = new SimpleStringProperty();
    
     public consultantScheduleModel() {}

    public consultantScheduleModel(int apptId, int id, String name, String date, String startT, String endT, String atitle, String alocation, String adescription, String atype, String acontact) {
        setCustId(id);
        setAppointmentId(apptId);
        setCustName(name);
        setCustDate(date);
        setStartTime(startT);
        setEndTime(endT);
        setTitle(atitle);
        setLocation(alocation);
        setDescription(adescription);
        setType(atype);
        setContact(acontact);
    }
    public void setAppointmentId(int apptId) {
        this.appointmentId.set(apptId);
    }
    
    public void setCustId(int custId) {
        this.custId.set(custId);
    }
    
    public void setCustName(String custName) {
        this.custName.set(custName);
    }
    public void setCustDate(String date) {
        this.custDate.set(date);
    }
    
    public void setStartTime(String time) {
        this.startTime.set(time);
    }
    public void setEndTime(String time) {
        this.endTime.set(time);
    }
    
    public void setTitle(String title) {
        this.title.set(title);
    }
    public void setLocation(String location) {
        this.location.set(location);
    }
    public void setDescription(String desc) {
        this.description.set(desc);
    }
    public void setType(String type) {
        this.type.set(type);
    }
    public void setContact(String contact) {
        this.contact.set(contact);
    }
    
    public int getAppointmentId() {
        return appointmentId.get();
    }
    
    public int getCustId() {
        return custId.get();
    }
    
    public String getCustName() {
        return custName.get();
    }
    public String getCustDate() {
        return custDate.get();
    }
    public String getStartTime() {
        return startTime.get();
    }
    public String getEndTime() {
        return endTime.get();
    }
    public String getTitle() {
        return title.get();
    }
    public String getLocation() {
        return location.get();
    }
    public String getDescription() {
        return description.get();
    }    
    public String getContact() {
        return contact.get();
    }
    public String getType() {
        return type.get();
    }
}
