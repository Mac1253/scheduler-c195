/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;

/**
 *
 * @author buger
 */
public class Logger {
    
    
    static public void logSignIn (int userId, String userName) {
        try {
            File logFile = new File("logger.txt");
            FileWriter fw = new FileWriter(logFile, true);
            if (logFile.exists()) {  
             fw.write("User: " + userName + " with userId: " + userId + " logged in at " +  LocalDateTime.now() +"\n");
             fw.close();   
            } 
        } catch (IOException e) {
            System.out.println(e);            
        }
    }
}
